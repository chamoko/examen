﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using SQLite;
using WP8Sqlite.Resources;
using WP8Sqlite.Model;

namespace WP8Sqlite
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();

            //lbVentas.SelectionChanged += lbVentas_SelectionChanged;
        }

        void lbVentas_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MessageBox.Show("Detalles de la venta");
        }


        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            lbVentas.ItemsSource = new Ventas().DameVentas();
        }

        private void BtnUpdate_OnClick(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AgregarProductos.xaml", UriKind.Relative));
        }

        private void BtnDelete_OnClick(object sender, RoutedEventArgs e)
        {
            
        }

        private void BtnInsert_OnClick(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AgregarVenta.xaml", UriKind.Relative));
            
        }
    }
}