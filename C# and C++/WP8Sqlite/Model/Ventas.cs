﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP8Sqlite.Model
{
    public class Venta
    {
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int Id { get; set; }
        public string Fecha { get; set; }
        public double Subtotal { get; set; }
        public double Iva { get; set; }
        public double Total { get; set; }
        public double Recibido { get; set; }
        public double Cambio { get; set; }
        [SQLite.Ignore]
        public List<DetalleVenta> DetalleVenta { get; set; }
    }

    public class Ventas
    {
        public List<Venta> DameVentas()
        {
            List<Venta> ventas;
            using (var db = new SQLiteConnection(App.dbPath))
            {
                ventas= db.Table<Venta>().ToList();
            }
            foreach(Venta v in ventas)
            {
                v.DetalleVenta = new DetallesVentas().DameDetalleVenta(v.Id);
            }
            return ventas;
        }

        public Venta DameVenta(int ID)
        {
            using (var db = new SQLiteConnection(App.dbPath))
            {
                var existing = db.Query<Venta>("select * from Venta where Id = " + ID.ToString()).FirstOrDefault();
                if (existing != null)
                {
                    existing.DetalleVenta = new DetallesVentas().DameDetalleVenta(existing.Id);
                    return existing;
                }
                return null;
            }
        }

        public void Inserta(Venta venta)
        {
            using (var db = new SQLiteConnection(App.dbPath))
            {
                db.RunInTransaction(() =>
                {
                    venta.Fecha = DateTime.Now.ToShortDateString();
                    db.Insert(venta);
                    
                });
                foreach (DetalleVenta det in venta.DetalleVenta)
                {
                    det.IdVenta = venta.Id;
                    new DetallesVentas().Inserta(det);
                }
            }
        }

        

        
    }
}
