﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP8Sqlite.Model
{
    public class Producto
    {
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int Id { get; set; }
        public string Nombre { get; set; }
        public double Precio { get; set; }
    }
    public class Productos
    {
         public List<Producto> DameProductos()
        {
            using (var db = new SQLiteConnection(App.dbPath))
            {
                return db.Table<Producto>().ToList();
            }
        }

         public Producto DameProducto(int ID)
         {
             using (var db = new SQLiteConnection(App.dbPath))
             {
                 var existing = db.Query<Producto>("select * from Producto where Id = " + ID.ToString()).FirstOrDefault();
                 if (existing != null)
                 {
                     return existing;
                 }
                 return null;
             }
         }

        public void Inserta(Producto producto)
         {
             using (var db = new SQLiteConnection(App.dbPath))
             {
                 db.RunInTransaction(() =>
                 {
                      db.Insert(new Producto() { Nombre = producto.Nombre, Precio = producto.Precio });
                 });
             }
         }

        public void Edita(int ID,Producto prod)
        {
            using (var db = new SQLiteConnection(App.dbPath))
            {
                var existing = db.Query<Producto>("select * from Producto where Id = " + ID.ToString()).FirstOrDefault();
                if (existing != null)
                {
                    existing.Nombre = prod.Nombre;
                    existing.Precio = prod.Precio;
                    db.RunInTransaction(() =>
                        {
                            db.Update(existing);
                        });
                }
            }
        }

        public void Elimina(int ID)
        {
            using (var db = new SQLiteConnection(App.dbPath))
            {
                var existing = db.Query<Producto>("select * from Prodcuto where Id = " + ID.ToString()).FirstOrDefault();
                if (existing != null)
                {
                    db.RunInTransaction(() =>
                    {
                        db.Delete(existing);
                    });
                }
            }
        }



    }
}
