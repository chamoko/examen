﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP8Sqlite.Model
{
    public class DetalleVenta
    {
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int Id { get; set; }
        public int IdVenta { get; set; }
        public int IdProducto { get; set; }
        public double Cantidad { get; set; }
        public double Importe { get; set; }
        [SQLite.Ignore]
        public Producto Producto { get; set; }

    }
    public class DetallesVentas
    {
        
        public List<DetalleVenta> DameDetalleVenta(int IDVenta)
        {
            using (var db = new SQLiteConnection(App.dbPath))
            {
                var existing = db.Query<DetalleVenta>("select * from DetalleVenta where IdVenta = " + IDVenta.ToString());
                if (existing != null)
                {
                    foreach (DetalleVenta det in existing)
                    {
                        det.Producto = new Productos().DameProducto(det.IdProducto);
                    }
                    return existing;
                }
                return null;
            }
        }

       

        public void Inserta(DetalleVenta venta)
        {
            using (var db = new SQLiteConnection(App.dbPath))
            {
                db.RunInTransaction(() =>
                {
                    db.Insert(new DetalleVenta() { IdVenta = venta.IdVenta, IdProducto = venta.IdProducto, Cantidad = venta.Cantidad, Importe = venta.Importe });
                });
            }
        }
    }
}
