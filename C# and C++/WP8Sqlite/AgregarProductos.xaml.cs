﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using WP8Sqlite.Model;

namespace WP8Sqlite
{
    public partial class AgregarProductos : PhoneApplicationPage
    {
        public AgregarProductos()
        {
            InitializeComponent();
        }
        private void CargaLista()
        {
            lbProductos.ItemsSource = new Productos().DameProductos();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            CargaLista();
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            new Productos().Inserta(new Producto() { Nombre=txtNombre.Text, Precio=Convert.ToDouble(txtPrecio.Text) });
            CargaLista();
        }
    }
}