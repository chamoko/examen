﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using WP8Sqlite.Model;
using Coding4Fun.Toolkit.Controls;
using System.Windows.Input;

namespace WP8Sqlite
{
    public partial class AgregarVenta : PhoneApplicationPage
    {
        Venta objVenta;
        public AgregarVenta()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            cmbProductos.ItemsSource = new Productos().DameProductos();
            objVenta = new Venta();
            objVenta.DetalleVenta = new List<DetalleVenta>();
       
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            double total=0;
            double subtotal=0;
            double impuesto=0;
            objVenta.DetalleVenta.Add(new DetalleVenta() 
            { 
                IdProducto = (cmbProductos.SelectedItem as Producto).Id, 
                Cantidad = Convert.ToDouble(txtCantidad.Text),
                Importe = (cmbProductos.SelectedItem as Producto).Precio * Convert.ToDouble(txtCantidad.Text),
                Producto = (cmbProductos.SelectedItem as Producto)
            });

            lbProductos.Items.Clear();
            foreach(DetalleVenta det in objVenta.DetalleVenta)
            {
                lbProductos.Items.Add(det);
                subtotal += det.Importe;
            }
            impuesto = subtotal * .16;
            total = subtotal + impuesto;
            txtImpuesto.Text = impuesto.ToString();
            txtSubtotal.Text = subtotal.ToString();
            txtTotals.Text = total.ToString();

            objVenta.Subtotal = subtotal;
            objVenta.Iva = impuesto;
            objVenta.Total = total;
            Conversion c = new Conversion();
            txtLetras.Text = "SON: "+ c.enletras(total.ToString());

        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            //new Ventas().Inserta(objVenta);

            InputPrompt input = new InputPrompt();
            input.InputScope = new InputScope { Names = { new InputScopeName() { NameValue = InputScopeNameValue.Number } } };
            input.Completed += input_Completed;
            input.Title = "Pagar la venta";
            input.Message = "Son $" + objVenta.Total.ToString() + " por favor intruzca el monto recibido.";
            input.Value = "";
            input.MessageTextWrapping = TextWrapping.Wrap;
            input.IsCancelVisible = true;
            input.Show();
            InputPrompt sendMail = new InputPrompt();
            sendMail.InputScope = new InputScope { Names = { new InputScopeName() { NameValue = InputScopeNameValue.Text } } };
            sendMail.Completed += sendMail_Completed;
            sendMail.Title = "Enviar por correo";
            sendMail.Message = "introduzca un Email Válido";
            sendMail.Value = "";
            sendMail.MessageTextWrapping = TextWrapping.Wrap;
            sendMail.IsCancelVisible = true;
            sendMail.Show();
        }
        void sendMail_Completed(object sender, PopUpEventArgs<string, PopUpResult> e)
        {
            string mail = e.Result;
            try
            {
                sendMail s = new sendMail(objVenta, mail);
            }
            catch (Exception ex)
            {

            }
        }
        void input_Completed(object sender, PopUpEventArgs<string, PopUpResult> e)
        {
            double recibido = Convert.ToDouble(e.Result);
            objVenta.Recibido = recibido;
            bool guarda = false;
            if(recibido == objVenta.Total)
            {
                objVenta.Cambio = 0;
                guarda = true;
            }
            else if (recibido > objVenta.Total)
            {
                objVenta.Cambio = objVenta.Total - recibido;
                MessageBox.Show("El monto de su cambio es $" + objVenta.Cambio);
                guarda = true;
            }
            else
            {
                MessageBox.Show("El monto recibido debe ser igual o mayor a $" + objVenta.Total );
                guarda = false;
            }

            if (guarda)
            {
                new Ventas().Inserta(objVenta);
                MessageBox.Show("Venta guardada correctemente");
                NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            }
        } 
        
    }
}