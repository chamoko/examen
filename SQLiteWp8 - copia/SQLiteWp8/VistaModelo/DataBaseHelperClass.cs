﻿using SQLite;
using SQLiteWp8.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace SQLiteWp8.VistaModelo
{
    public class DataBaseHelperClass
    {
        SQLiteConnection dbCon;

        public async Task<bool> onCreate(string BD)
        {
            try
            {
                if (!ChecaExisteBD("Contactos.sqlite").Result)
                {
                    using (var db = new SQLiteConnection(BD))
                    {
                        db.CreateTable<Contacts>();
                    }             
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private async Task<bool> ChecaExisteBD(string filename)
        {
            try
            {
                var store = await ApplicationData.Current.LocalFolder.GetFileAsync(filename);
                return true;
            }
            catch
            {
            }
            return false;
        }

        public Contacts DameContacto(int id)
        {
            using(dbCon = new SQLiteConnection(App.BD))
            {
                return dbCon.Query<Contacts>("select * from Contacts where Id = " + id).FirstOrDefault();
            }
        }

        public List<Contacts> DameTodosContactos()
        {
            using (dbCon = new SQLiteConnection(App.BD))
            {
                return dbCon.Query<Contacts>("select * from Contacts");
            }
        }

        public bool ActualizaContacto(Contacts c)
        {
            using (dbCon = new SQLiteConnection(App.BD))
            {
                Contacts contacto = dbCon.Query<Contacts>("select * from Contacts where Id = " + c.Id).FirstOrDefault();
                if(contacto != null)
                {
                    contacto.Name = c.Name;
                    contacto.PhoneNumber = c.PhoneNumber;
                    contacto.CreationDate = contacto.CreationDate;
                    int res = 0;
                    dbCon.RunInTransaction(() =>
                        {
                           res = dbCon.Update(contacto);
                        });
                    return res > 0;
                }
                return false;
            }
        }

        public bool Inserta(Contacts c)
        {
            using (dbCon = new SQLiteConnection(App.BD))
            {
                if (c != null)
                {                 
                    int res = 0;
                    dbCon.RunInTransaction(() =>
                    {
                        res = dbCon.Insert(c);
                    });
                    return res > 0;
                }
                return false;
            }
        }

        public bool BorraContacto(int id)
        {
            using (dbCon = new SQLiteConnection(App.BD))
            {
                Contacts contacto = dbCon.Query<Contacts>("select * from Contacts where Id = " + id).FirstOrDefault();
                if (contacto != null)
                {
                    
                    int res = 0;
                    dbCon.RunInTransaction(() =>
                    {
                        res = dbCon.Delete(contacto);
                    });
                    return res > 0;
                }
                return false;
            }
        }

        public bool BorraTodosContactos()
        {
            try
            {
                using (dbCon = new SQLiteConnection(App.BD))
                {
                    dbCon.DropTable<Contacts>();
                    dbCon.CreateTable<Contacts>();
                    dbCon.Dispose();
                    dbCon.Close();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        
    }
}
