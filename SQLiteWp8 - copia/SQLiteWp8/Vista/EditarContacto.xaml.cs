﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using SQLiteWp8.Modelo;
using SQLiteWp8.VistaModelo;

namespace SQLiteWp8.Vista
{
    public partial class EditarContacto : PhoneApplicationPage
    {
        Contacts objContacto;
        public EditarContacto()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            objContacto = new DataBaseHelperClass().DameContacto(int.Parse(NavigationContext.QueryString["id"]));
            ContentPanel.DataContext = objContacto;
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if(MessageBox.Show("¿Seguro que desea eliminar al contacto " + objContacto.Name + "?","AVISO",MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                if (new DataBaseHelperClass().BorraContacto(objContacto.Id))
                {
                    MessageBox.Show("Contacto eliminado", "AVISO", MessageBoxButton.OK);
                    NavigationService.Navigate(new Uri("/Vista/LeeTodosContactos.xaml", UriKind.Relative));
                }
                else
                    MessageBox.Show("No se pudo eliminar al contacto", "AVISO", MessageBoxButton.OK);
            }
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (new DataBaseHelperClass().ActualizaContacto(objContacto))
            {
                MessageBox.Show("Contacto editado", "AVISO", MessageBoxButton.OK);
                NavigationService.Navigate(new Uri("/Vista/LeeTodosContactos.xaml", UriKind.Relative));
            }
            else
                MessageBox.Show("No se pudo editar el contacto", "AVISO", MessageBoxButton.OK);
        }
    }
}