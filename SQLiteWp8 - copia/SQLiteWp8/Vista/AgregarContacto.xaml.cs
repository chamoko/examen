﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using SQLiteWp8.VistaModelo;
using SQLiteWp8.Modelo;

namespace SQLiteWp8.Vista
{
    public partial class AgregarContacto : PhoneApplicationPage
    {
        public AgregarContacto()
        {
            InitializeComponent();
        }

        private async void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            DataBaseHelperClass dbhelper = new DataBaseHelperClass();
            if(!string.IsNullOrWhiteSpace(txtNombre.Text) && !string.IsNullOrWhiteSpace(txtTelefono.Text))
            {
                if(dbhelper.Inserta(new Contacts(txtNombre.Text, txtTelefono.Text)))
                {
                    MessageBox.Show("Contacto guardado orrectamente", "AVISO", MessageBoxButton.OK);
                    NavigationService.Navigate(new Uri("/Vista/LeeTodosContactos.xaml", UriKind.Relative));
                }
                else
                    MessageBox.Show("No se pudo guardar el contacto", "AVISO", MessageBoxButton.OK);
            }
            else
                MessageBox.Show("LLene todos los campos.", "AVISO", MessageBoxButton.OK);
        }

       
    }
}