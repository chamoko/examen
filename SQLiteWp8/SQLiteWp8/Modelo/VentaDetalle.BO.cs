﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlador.BO;

namespace Controlador.BO
{
    class VentaDetalle
    {
        private int Id;
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int id
        {
            get { return this.Id; }
            set { this.Id = value; }
        }
        private int IdVentaDetalle;
        public int idVentaDetalle
        {
            get { return this.IdVentaDetalle; }
            set { this.IdVentaDetalle = value; }
        }
        private List<Producto> ListaProductos;
        public List<Producto> listaProductos
        {
            set { this.ListaProductos = value; }
            get { return this.ListaProductos; }
        }
    }
}
