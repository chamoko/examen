﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlador.BO
{
    public class Producto
    {
        private int Id;
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int id
        {
            set { this.Id = value; }
            get { return this.Id; }
        }
        private string Nombre;
        public string nombre
        {
            set { this.Nombre = value; }
            get { return this.Nombre; }
        }
        private float Precio;
        public float precio
        {
            set { this.Precio = value; }
            get { return this.Precio; }
        }
        private string Descripcion;
        public string descripcion
        {
            set { this.Descripcion = value; }
            get { return this.Descripcion; }
        }
    }
}
