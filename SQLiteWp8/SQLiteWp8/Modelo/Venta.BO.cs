﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlador.BO
{
    class Venta
    {
        private int Id;
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int id
        {
            get { return this.Id; }
            set { this.Id = value; }
        }
        private string Fecha;
        public string fecha
        {
            get { return this.Fecha; }
            set { this.Fecha = value; }
        }
        private float Total;
        public float total
        {
            get { return this.Total; }
            set { this.Total = value; }
        }
        private float Subtotal;
        public float subtotal
        {
            get { return this.Subtotal; }
            set { this.Subtotal = value; }
        }
        private float Iva;
        public float iva
        {
            get { return this.Iva; }
            set { this.Iva = value; }
        }
        private float Recibe;
        public float recibe
        {
            get { return this.Recibe; }
            set { this.Recibe = value; }
        }
        private float Devuelve;
        public float devuelve
        {
            get { return this.Devuelve; }
            set { this.Devuelve = value; }
        }
    }
}
