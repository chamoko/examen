﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using SQLite;
using Controlador.BO;
using Controlador.Controller;

namespace Controlador.Controller
{
    class DataBaseHelperClass
    {
        SQLiteConnection dbCon;

        public async Task<bool> onCreate(string BD)
        {
            try
            {
                if (!ChecaExisteBD("Venta.sqlite").Result)
                {
                    using (var db = new SQLiteConnection(BD))
                    {
                        db.CreateTable<Venta>();
                        db.CreateTable<Producto>();
                        db.CreateTable<VentaDetalle>();
                    }             
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private async Task<bool> ChecaExisteBD(string filename)
        {
            try
            {
                var store = await ApplicationData.Current.LocalFolder.GetFileAsync(filename);
                return true;
            }
            catch
            {
            }
            return false;
        }

        public Venta GetVenta(int id)
        {
            using(dbCon = new SQLiteConnection(App.BD))
            {
                return dbCon.Query<Venta>("select * from Venta where Id = " + id).FirstOrDefault();
            }
        }
        public VentaDetalle GetVDetalle(int id)
        {
            using(dbCon = new SQLiteConnection(App.BD))
            {
                return dbCon.Query<VentaDetalle>("select * from VentaDetalle where Id = " + id).FirstOrDefault();
            }
        }
        public List<Producto> GetAllProductos ()
        {
            using(dbCon = new SQLiteConnection(App.BD))
            {
                return dbCon.Query<Producto>("select * from Producto");
            }
        }
        public List<Venta> GetAllVentas()
        {
            using (dbCon = new SQLiteConnection(App.BD))
            {
                return dbCon.Query<Venta>("select * from Venta");
            }
        }

        public bool ActualizaVenta(Venta v)
        {
            using (dbCon = new SQLiteConnection(App.BD))
            {
                Venta venta = dbCon.Query<Venta>("select * from Contacts where Id = " + v.id).FirstOrDefault();
                if(venta != null)
                {
                    venta.devuelve = v.devuelve;
                    venta.fecha=v.fecha;
                    venta.iva = v.iva;
                    venta.recibe=v.recibe;
                    venta.subtotal=v.subtotal;
                    venta.total=v.total;
                    int res = 0;
                    dbCon.RunInTransaction(() =>
                        {
                           res = dbCon.Update(venta);
                        });
                    return res > 0;
                }
                return false;
            }
        }

        public bool Inserta(Venta v)
        {
            using (dbCon = new SQLiteConnection(App.BD))
            {
                if (v != null)
                {                 
                    int res = 0;
                    dbCon.RunInTransaction(() =>
                    {
                        res = dbCon.Insert(v);
                    });
                    return res > 0;
                }
                return false;
            }
        }

       

   
        
    }
}



