﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Controlador.Controller;
using SQLiteWp8.Modelo;

namespace SQLiteWp8.Vista
{
    public partial class LeeTodosContactos : PhoneApplicationPage
    {
        public LeeTodosContactos()
        {
            InitializeComponent();
        }

        
        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(lista.SelectedItem != null)
            {
                Contacts contac = lista.SelectedItem as Contacts;
                NavigationService.Navigate(new Uri("/View/EditarContacto.xaml?id="+contac.Id, UriKind.Relative));
            }
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            Controlador.Controller.DataBaseHelperClass bdhelper = new DataBaseHelperClass();
            lista.ItemsSource = bdhelper.GetAllVentas();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/AgregarContacto.xaml", UriKind.Relative));
        }

        private void btnBoorartodo_Click(object sender, RoutedEventArgs e)
        {
            DataBaseHelperClass bdhelper = new DataBaseHelperClass();
            //bdhelper.BorraTodosContactos();
            lista.Items.Clear();
        }

       
    }
}